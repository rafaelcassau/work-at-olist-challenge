from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

from rest_framework_swagger.views import get_swagger_view


schema_view = get_swagger_view(title='Work at olist API')

urlpatterns = [
    path('', RedirectView.as_view(permanent=True, url='/docs/')),
    path('admin/', admin.site.urls),
    path(r'api/', include('core.urls')),
    path(
        r'api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
    # docs
    path(r'docs/', schema_view)
]

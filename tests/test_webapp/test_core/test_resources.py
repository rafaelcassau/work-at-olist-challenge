from decimal import Decimal
from datetime import date, time, timedelta

import pytest

from hamcrest import assert_that, has_entries

from rest_framework.reverse import reverse

from core.models import Bill
from common.utils import get_last_close_period


pytestmark = pytest.mark.django_db


# http verbs not allowed


def test_records_get_not_allowed(api_client):
    url = reverse('records-list')
    response = api_client.get(url)
    assert response.status_code == 405


def test_records_put_not_allowed(api_client):
    url = reverse('records-list')
    response = api_client.put(url)
    assert response.status_code == 405


def test_records_patch_not_allowed(api_client):
    url = reverse('records-list')
    response = api_client.patch(url)
    assert response.status_code == 405


def test_records_delete_not_allowed(api_client):
    url = reverse('records-list')
    response = api_client.delete(url)
    assert response.status_code == 405


def test_bills_post_not_allowed(api_client):
    url = reverse('bills-list')
    response = api_client.post(url)
    assert response.status_code == 405


def test_bills_put_not_allowed(api_client):
    url = reverse('bills-list')
    response = api_client.put(url)
    assert response.status_code == 405


def test_bills_patch_not_allowed(api_client):
    url = reverse('bills-list')
    response = api_client.patch(url)
    assert response.status_code == 405


def test_bills_delete_not_allowed(api_client):
    url = reverse('bills-list')
    response = api_client.delete(url)
    assert response.status_code == 405


# start record

def test_add_start_record_success(api_client):
    url = reverse('records-list')

    data = {
        "type": "start",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)

    assert response.status_code == 201


def test_add_start_record_invalid_type(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_start_record_invalid_timestamp(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T25:00:00Z",
        "call_id": 70,
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_start_record_invalid_call_id(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T25:00:00Z",
        "call_id": 'invalid-call-id',
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_start_record_invalid_source(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T25:00:00Z",
        "call_id": 'invalid-call-id',
        "source": "999885264",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_start_record_invalid_destination(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T25:00:00Z",
        "call_id": 'invalid-call-id',
        "source": "99988526433",
        "destination": "9993468"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_start_record_source_is_required_record_start(api_client):
    url = reverse('records-list')

    data = {
        "type": "start",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "destination": "99988526433",
    }

    response = api_client.post(url, data=data)

    assert response.status_code == 400


def test_add_start_record_destination_is_required_record_start(api_client):
    url = reverse('records-list')

    data = {
        "type": "start",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "source": "99988526433",
    }

    response = api_client.post(url, data=data)

    assert response.status_code == 400


def test_add_start_record_duplicated_call_id_and_type(
    api_client,
    record_event_start
):
    url = reverse('records-list')

    data = {
        "type": "start",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)

    assert response.status_code == 400


# end record

def test_add_end_record_success(api_client):
    url = reverse('records-list')

    data = {
        "type": "end",
        "timestamp": "2016-02-29T14:00:00Z",
        "call_id": 70,
    }

    response = api_client.post(url, data=data)

    assert response.status_code == 201


def test_add_end_record_invalid_type(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T14:00:00Z",
        "call_id": 70,
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_end_record_invalid_timestamp(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T25:00:00Z",
        "call_id": 70,
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_end_record_invalid_call_id(api_client):
    url = reverse('records-list')

    data = {
        "type": "type-does-not-exit",
        "timestamp": "2016-02-29T25:00:00Z",
        "call_id": 'invalid-call-id',
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 400


def test_add_end_record_duplicated_call_id_and_type(
    api_client,
    record_event_end
):
    url = reverse('records-list')

    data = {
        "type": "end",
        "timestamp": "2016-02-29T14:00:00Z",
        "call_id": 70,
    }

    response = api_client.post(url, data=data)

    assert response.status_code == 400


# start and end record end to end test


def test_add_couple_start_and_end_record_events(
    api_client,
    call_price_kind_standard,
    call_price_kind_reduced
):
    url = reverse('records-list')

    data = {
        "type": "start",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 201

    data = {
        "type": "end",
        "timestamp": "2016-02-29T14:00:00Z",
        "call_id": 70,
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 201

    bill = Bill.objects.get()

    assert bill.year == 2016
    assert bill.month == 2
    assert bill.subscriber == '99988526423'
    assert bill.calls.count() == 1

    bill_record = bill.calls.first()

    assert bill_record.destination == '9993468278'
    assert bill_record.start_date == date(2016, 2, 29)
    assert bill_record.start_time == time(12, 0)
    assert bill_record.duration == timedelta(hours=2)


def test_add_couple_end_and_start_record_events(
    api_client,
    call_price_kind_standard,
    call_price_kind_reduced
):
    url = reverse('records-list')

    data = {
        "type": "end",
        "timestamp": "2016-02-29T14:00:00Z",
        "call_id": 70,
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 201

    data = {
        "type": "start",
        "timestamp": "2016-02-29T12:00:00Z",
        "call_id": 70,
        "source": "99988526423",
        "destination": "9993468278"
    }

    response = api_client.post(url, data=data)
    assert response.status_code == 201

    bill = Bill.objects.get()

    assert bill.year == 2016
    assert bill.month == 2
    assert bill.subscriber == '99988526423'
    assert bill.calls.count() == 1

    bill_record = bill.calls.first()

    assert bill_record.destination == '9993468278'
    assert bill_record.start_date == date(2016, 2, 29)
    assert bill_record.start_time == time(12, 0)
    assert bill_record.duration == timedelta(hours=2)
    assert bill_record.price == Decimal('11.16')


# get bill records

def test_get_bill_records_subscriber_is_required(api_client, bill_02_2016):
    url = reverse('bills-list')

    response = api_client.get(url)

    assert response.status_code == 400


def test_get_bill_records_subscriber_is_invalid(api_client, bill_02_2016):
    url = reverse('bills-list')

    response = api_client.get(url, {'subscriber': '123456'})

    assert response.status_code == 400


def test_get_bill_records_subscriber_not_found(api_client, bill_02_2016):
    url = reverse('bills-list')

    response = api_client.get(url, {'subscriber': '16999990000'})

    assert response.status_code == 200
    assert response.data == []


def test_get_bill_records_last_close_month_empty(api_client, bill_02_2016):
    url = reverse('bills-list')

    response = api_client.get(url, {'subscriber': '99988526433'})

    assert response.status_code == 200
    assert response.data == []


def test_get_bill_records_last_close_month(api_client, bill_last_close_month):
    url = reverse('bills-list')

    response = api_client.get(url, {'subscriber': '99988526423'})

    last_close_period = get_last_close_period()
    last_close_period = date(
        year=last_close_period.year,
        month=last_close_period.month,
        day=1
    )

    assert response.status_code == 200
    assert len(response.data) == 1

    assert_that(
        response.data[0], has_entries({
            'destination': '9993468278',
            'start_date': last_close_period.strftime('%Y-%m-%d'),
            'start_time': '12:00:00',
            'duration': '02:00:00',
            'price': '11.16',
        })
    )


def test_get_bill_records_last_close_month_ignore_current_bill(
    api_client,
    bill_last_close_month,
    bill_current_month
):
    url = reverse('bills-list')

    response = api_client.get(url, {'subscriber': '99988526423'})

    last_close_period = get_last_close_period()
    last_close_period = date(
        year=last_close_period.year,
        month=last_close_period.month,
        day=1
    )

    assert response.status_code == 200
    assert len(response.data) == 1

    assert_that(
        response.data[0], has_entries({
            'destination': '9993468278',
            'start_date': last_close_period.strftime('%Y-%m-%d'),
            'start_time': '12:00:00',
            'duration': '02:00:00',
            'price': '11.16',
        })
    )


def test_get_bill_records_with_period_type_02_2016(
    api_client,
    bill_02_2016,
    bill_12_2017,
    bill_03_2018
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': '02/2016'
    })

    assert response.status_code == 200
    assert len(response.data) == 1

    assert_that(
        response.data[0], has_entries({
            'destination': '9993468278',
            'start_date': '2016-02-29',
            'start_time': '12:00:00',
            'duration': '02:00:00',
            'price': '11.16',
        })
    )


def test_get_bill_records_period_type_02_2016_not_found(
    api_client,
    bill_12_2017,
    bill_03_2018
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': '02/2016'
    })

    assert response.status_code == 200
    assert response.data == []


def test_get_bill_records_with_period_type_12_2017(
    api_client,
    bill_02_2016,
    bill_12_2017,
    bill_03_2018
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': '12/2017'
    })

    assert response.status_code == 200
    assert len(response.data) == 6

    assert_that(
        response.data[0], has_entries({
            'destination': '9993468278',
            'start_date': '2017-12-12',
            'start_time': '22:47:00',
            'duration': '00:03:00',
            'price': '0.36',
        })
    )


def test_get_bill_records_period_type_12_2017_not_found(
    api_client,
    bill_02_2016,
    bill_03_2018
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': '12/2017'
    })

    assert response.status_code == 200
    assert response.data == []


def test_get_bill_records_with_period_type_03_2018(
    api_client,
    bill_02_2016,
    bill_12_2017,
    bill_03_2018
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': '03/2018'
    })

    assert response.status_code == 200
    assert len(response.data) == 1

    assert_that(
        response.data[0], has_entries({
            'destination': '9993468278',
            'start_date': '2018-02-28',
            'start_time': '21:57:00',
            'duration': '1 00:13:43',
            'price': '131.13',
        })
    )


def test_get_bill_records_period_type_03_2018_not_found(
    api_client,
    bill_02_2016,
    bill_12_2017
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': '03/2018'
    })

    assert response.status_code == 200
    assert response.data == []


def test_get_bill_records_with_invalid_period_type(
    api_client,
    bill_last_close_month
):
    url = reverse('bills-list')

    response = api_client.get(url, {
        'subscriber': '99988526423',
        'period': 'aa/2019'
    })

    assert response.status_code == 400

from decimal import Decimal
from datetime import date, time, timedelta

import pytest

from django.utils import timezone

from rest_framework.test import APIClient

from common.utils import get_last_close_period

from .factories import (
    RecordEventFactory,
    CallPriceKindFactory,
    BillFactory,
    BillCallRecordFactory,
    UserFactory
)


# TODO check why Factory.create() isn't save instance
# entities

@pytest.fixture
def record_event_start():
    record_start = RecordEventFactory.build(
        source='99988526423',
        destination='9993468278'
    )
    record_start.save()

    return record_start


@pytest.fixture
def record_event_end():
    record_end = RecordEventFactory.build(
        type='end',
        timestamp='2016-02-29T14:00:00Z'
    )
    record_end.save()

    return record_end


@pytest.fixture
def call_price_kind_standard():
    call_price_kind = CallPriceKindFactory.build()
    call_price_kind.save()

    return call_price_kind


@pytest.fixture
def call_price_kind_reduced():
    start_time = time(hour=22, minute=0, second=0)
    end_time = time(hour=6, minute=0, second=0)
    minute_price = Decimal('0')

    call_price_kind = CallPriceKindFactory.build(
        start_time=start_time,
        end_time=end_time,
        minute_price=minute_price
    )
    call_price_kind.save()

    return call_price_kind


@pytest.fixture
def bill_02_2016(call_price_kind_standard):
    bill = BillFactory.build()
    bill.save()

    bill_record = BillCallRecordFactory.build(
        bill=bill,
        call_price_kind=call_price_kind_standard
    )
    bill_record.save()

    return bill


@pytest.fixture
def bill_12_2017(call_price_kind_standard, call_price_kind_reduced):
    bill = BillFactory.build(year=2017, month=12)
    bill.save()

    bill_record_list = [
        {
            'duration': timedelta(hours=1, minutes=13, seconds=43),
            'price': Decimal('0.36'),
            'start_time': time(hour=4, minute=57),
            'call_price_kind': call_price_kind_reduced,
        },
        {
            'duration': timedelta(minutes=7, seconds=43),
            'price': Decimal('0.99'),
            'start_time': time(hour=15, minute=7),
            'call_price_kind': call_price_kind_standard,
        },
        {
            'duration': timedelta(minutes=4, seconds=58),
            'price': Decimal('0.72'),
            'start_time': time(hour=15, minute=7),
            'call_price_kind': call_price_kind_standard,
        },
        {
            'duration': timedelta(seconds=0),
            'price': Decimal('0.36'),
            'start_time': time(hour=21, minute=57),
            'call_price_kind': call_price_kind_standard,
        },

        {
            'duration': timedelta(days=1, hours=0, minutes=13, seconds=43),
            'price': Decimal('131.13'),
            'start_time': time(hour=21, minute=57),
            'call_price_kind': call_price_kind_standard,
        },
        {
            'duration': timedelta(minutes=3),
            'price': Decimal('0.36'),
            'start_time': time(hour=22, minute=47),
            'call_price_kind': call_price_kind_reduced,
        },
    ]

    for bill_record_data in bill_record_list:
        start_date = date(day=12, month=12, year=2017)
        bill_record = BillCallRecordFactory.build(
            start_date=start_date,
            bill=bill,
            **bill_record_data
        )
        bill_record.save()

    return bill


@pytest.fixture
def bill_03_2018(call_price_kind_standard):
    bill = BillFactory.build(year=2018, month=3)
    bill.save()

    bill_record = BillCallRecordFactory.build(
        duration=timedelta(days=1, hours=0, minutes=13, seconds=43),
        price=Decimal('131.13'),
        start_date=date(day=28, month=2, year=2018),
        start_time=time(hour=21, minute=57),
        bill=bill,
        call_price_kind=call_price_kind_standard,
    )
    bill_record.save()

    return bill


@pytest.fixture
def bill_last_close_month(call_price_kind_standard):
    last_close_period = get_last_close_period()

    bill = BillFactory.build(
        year=last_close_period.year,
        month=last_close_period.month
    )
    bill.save()

    start_date = date(
        year=last_close_period.year,
        month=last_close_period.month,
        day=1
    )

    bill_record = BillCallRecordFactory.build(
        start_date=start_date,
        bill=bill,
        call_price_kind=call_price_kind_standard,
    )
    bill_record.save()

    return bill


@pytest.fixture
def bill_current_month(call_price_kind_standard):
    today = timezone.now().date()

    bill = BillFactory.build(year=today.year, month=today.month)
    bill.save()

    bill_record = BillCallRecordFactory.build(
        start_date=date(year=today.year, month=today.month, day=1),
        bill=bill,
        call_price_kind=call_price_kind_standard,
    )
    bill_record.save()

    return bill


# user

@pytest.fixture
def user():
    user = UserFactory.build()
    user.save()

    return user


# client

@pytest.fixture
def api_client(user):
    client = APIClient()
    client.force_authenticate(user=user)

    return client

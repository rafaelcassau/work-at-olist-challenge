from decimal import Decimal
from datetime import date, time, timedelta

from django.contrib.auth.models import User

import factory

from core.models import RecordEvent, CallPriceKind, Bill, BillCallRecord


class RecordEventFactory(factory.Factory):

    class Meta:
        model = RecordEvent

    type = 'start'
    call_id = 70
    timestamp = '2016-02-29T12:00:00Z'
    source = None
    destination = None


class CallPriceKindFactory(factory.Factory):

    class Meta:
        model = CallPriceKind

    kind = 'standard'
    start_time = time(hour=6, minute=0, second=0)
    end_time = time(hour=22, minute=0, second=0)
    minute_price = Decimal('0.09')
    call_start_price = Decimal('0.36')


class BillFactory(factory.Factory):

    class Meta:
        model = Bill

    subscriber = '99988526423'
    year = 2016
    month = 2


class BillCallRecordFactory(factory.Factory):

    class Meta:
        model = BillCallRecord

    destination = '9993468278'
    start_date = date(2016, 2, 29)
    start_time = time(12, 0)
    duration = timedelta(hours=2)
    price = Decimal('11.16')
    call_id = 70
    bill = None
    call_price_kind = None


class UserFactory(factory.Factory):

    class Meta:
        model = User

    username = 'admin'
    first_name = 'admin'
    last_name = 'admin'
    email = 'admin@admin.com.br'
    is_staff = True
    is_superuser = True
    is_active = True
    password = '123'

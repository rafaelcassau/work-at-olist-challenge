from enum import Enum
from datetime import timedelta
from collections import namedtuple

from django.utils import timezone


class RecordTypeEnum(Enum):
    START = 'start'
    END = 'end'


def get_last_close_period():
    today = timezone.now().date()
    modified_date = today.replace(day=1)
    modified_date = modified_date - timedelta(days=1)

    Period = namedtuple('Period', ('month', 'year'))
    period = Period(month=modified_date.month, year=modified_date.year)

    return period

import re
from collections import namedtuple

from django.utils import timezone
from django.utils.translation import gettext as _

from rest_framework import serializers


class PhoneNumberField(serializers.CharField):

    def to_internal_value(self, value):
        exp = r'^(\d{2}\d{4,5}\d{4})$'
        regex = re.compile(exp)

        if not re.match(regex, value):
            raise serializers.ValidationError(
                _('A valid phone number is required.')
            )

        return value


class MonthYearField(serializers.CharField):

    def to_internal_value(self, value):
        exp = r'^(\d{2}/\d{4})$'
        regex = re.compile(exp)

        if not re.match(regex, value):
            raise serializers.ValidationError(_('A valid period is required.'))

        month, year = value.split('/')
        month = int(month)
        year = int(year)

        current_year = timezone.now().date().year
        if not (1 <= month <= 12) or not (1900 <= year <= current_year):
            raise serializers.ValidationError(_('A valid period is required.'))

        Period = namedtuple('Period', ('month', 'year'))
        period = Period(month=month, year=year)

        return period

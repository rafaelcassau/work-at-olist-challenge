activate:
	@echo "--> activate virtualenv"
	pipenv shell

develop:
	@echo "--> build"
	pipenv install --dev

up:
	@echo "--> start webapp"
	./manage.py runserver --settings=webapp.settings.development

stop:
	@echo "--> stop webapp"
	pkill -f runserver

superuser:
	@echo "--> create createsuperuser for API and Admin"
	./manage.py createsuperuser --settings=webapp.settings.development

migrate:
	@echo "--> generate new migrations and apply them"
	./manage.py makemigrations --settings=webapp.settings.development
	./manage.py migrate --settings=webapp.settings.development

shell:
	@echo "--> into django shell"
	./manage.py shell --settings=webapp.settings.development

test:
	@echo "--> running tests"
	py.test -s -x tests/

deploy:
	@echo "--> deploy app on heroku"
	git commit --allow-empty -m "empty commit"
	git push heroku master
	

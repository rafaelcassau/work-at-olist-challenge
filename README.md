# Work at Olist Webapp

## Description

>This is the basic application that implement the challenge requirements: https://github.com/olist/work-at-olist.

This application consists in a REST API that receive `start` and `end` calls records to process and store billing information given a subscriber.:

The application has two main endpoints:

```
/<base_url>/api/records/
```

This endpoint allows external resources to send call event records, this records can be `start record` or `end record` and the system gonna process the record only the both records are received and match by the `type` and `call_id`

Every time that a new record pair is stored, a bill record gonna be created and will be related with a bill period, a bill period consist in a period `(month/year)` and a `subscribe`

The entire billing process is asynchronous and decouple, this provide more strongly and trusted environment that can retry the whole process again in case of some failure.

### Example to send records

Call Start Record

```
/<base_url>/api/records/

{
    "type": "start",
    "timestamp": "2016-02-29T12:00:00Z",
    "call_id": 70,
    "source": "99988526423",
    "destination": "9993468278"
}
```

Call End Record

```
/<base_url>/api/records/

{
    "type": "end",
    "timestamp": "2016-02-29T14:00:00Z",
    "call_id": 70
}
```

```
/<base_url>/api/bills/?subscriber=<phone_number>&period=<period>
```

This endpoint will find bill records by closest month given a `subscriber` (phone number). If a `period` (month/year) is given then the custom bill period will be get if it's exists and is not current month of course.

### Example to get bill records

```
/<base_url>/api/bills/?subscriber=99988526423&period=02/2016
[
    {
        "destination": "9993468278",
        "start_date": "2016-02-29",
        "start_time": "12:00:00",
        "duration": "02:00:00",
        "price": "11.16"
    }
]
```

### Authentication

To use endpoints an authenticated user is required.
`Admin user` and `API user` are the same.

* Admin
```
URL: https://work-at-olist-cassau.herokuapp.com/admin
USERNAME: workatolist
PASSWORD: challenge
```

* API
```
URL: https://work-at-olist-cassau.herokuapp.com/api-auth/login/
USERNAME: workatolist
PASSWORD: challenge
```

### Admin Resources

* Resources that can be handle:
    * Can list, detail and filter record events.
    * Can list, detail and filter bills.
    * Can list, detail and filter bill records.
    * Can list, detail, add, update and delete call prices.

### Docs
```
URL: https://work-at-olist-cassau.herokuapp.com/docs/
```

## Requirements

To run the project in your local machine some thid party apps is required.

System third apps.
```
postgress >= 9.0
redis-server >= 4.0
```

Pyhon version.
```
python >= 3.6.0
``` 

## Running project in your local machine

* Pipenv get started
    * https://github.com/pypa/pipenv


* After install and configure your environment with requirements described above follow the instructions below:
    * With postgres up, create database `webapp` for the app.
    * Clone project `git clone git@bitbucket.org:rafaelcassau/work-at-olist-challenge.git`.
    * Into project root run `make develop` to install all project dependencies.
    * Active your virtualenv running the command `pipenv shell`.
    * Update `development.py` `(settings.DATABASES)` credentials.
    * To create and apply migrations run `make migrate`.
    * To create a superuser run `make superuser` and fill the fields.
    * To up project run `make up`.
    * To stop project run `make stop`.
    * To into in django shell execute `make shell`.

### Running tests.
* To run tests execute `make test`.

## TODOs

* I developed only end to end tests to consume less time, but a lot of unit tests need to be developed.

* Dockerize webapp.

* Configure a CI plataform

## Deploy on heroku

* To deploy this app on heroku follow the tutorials below: 
    * https://devcenter.heroku.com/articles/deploying-python
    * https://devcenter.heroku.com/articles/getting-started-with-python
    * https://devcenter.heroku.com/articles/celery-heroku

## Meta:

* This project was developed with Macbook (MacOS Sierra), postgres 10, redis 4, python 3.7.0 and VSCode text editor.

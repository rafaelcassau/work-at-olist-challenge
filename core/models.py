from decimal import Decimal
from django.db import models
from django.utils.translation import gettext as _

from common.utils import RecordTypeEnum

from .signals import process_record_signal
from .managers import RecordEventManager, BillManager, CallPriceKindManager


class RecordEvent(models.Model):
    KINDS = (
        (RecordTypeEnum.START.name.lower(), _('start')),
        (RecordTypeEnum.END.name.lower(), _('end')),
    )
    type = models.CharField(
        _('type'),
        choices=KINDS,
        max_length=8,
        db_index=True
    )
    call_id = models.IntegerField(_('call id'), db_index=True)
    timestamp = models.DateTimeField(_('timestamp'))
    source = models.CharField(
        _('origin phone number'),
        max_length=16,
        null=True,
        default=None
    )
    destination = models.CharField(
        _('destination phone number'),
        max_length=16,
        null=True,
        default=None
    )

    objects = RecordEventManager()

    class Meta:
        unique_together = ('type', 'call_id',)
        verbose_name = _('record')
        verbose_name_plural = _('records')

    def save(self, *args, **kwargs):
        is_new = not self.pk
        super().save(*args, **kwargs)
        if is_new:
            process_record_signal.send(sender=RecordEvent, record=self)

    def __str__(self):
        return f'{self.pk}'

    def __repr__(self):
        return f'<{self.pk}-{self.call_id}-{self.type}>'


class Bill(models.Model):
    subscriber = models.CharField(_('subscriber'), max_length=16)
    year = models.PositiveIntegerField(_('year'))
    month = models.PositiveIntegerField(_('month'))

    objects = BillManager()

    class Meta:
        verbose_name = _('bill')
        verbose_name_plural = _('bills')

    def __str__(self):
        return f'{self.pk}'

    def __repr__(self):
        return f'<{self.pk}-{self.subscriber}>'

    def records_already_processed(self, call_id):
        already_processed = self.calls.filter(call_id=call_id).exists()
        return already_processed

    def add_record(self, start_record, end_record):
        call_id = start_record.call_id
        destination = start_record.destination
        start_date = start_record.timestamp.date()
        start_time = start_record.timestamp.time()

        start_timestamp = start_record.timestamp
        end_timestamp = end_record.timestamp

        if self.records_already_processed(call_id):
            logger.info(
                f'bill.add_record - already exists a bill call' +
                f'record with call_id: {call_id}'
            )
            return

        call_price = CallPriceKind.objects.get_call_price_kind(start_time)
        if not call_price:
            logger.warning(
                f'bill.add_record - any call kind price was found.' +
                f'for time {start_time}'
            )
            return

        duration = end_timestamp - start_timestamp
        price = call_price.calculate_price(duration)

        bill_record = self.calls.create(
            destination=destination,
            start_date=start_date,
            start_time=start_time,
            duration=duration,
            price=price,
            call_id=call_id,
            call_price_kind=call_price,
        )
        return bill_record


class BillCallRecord(models.Model):
    destination = models.CharField(_('destination'), max_length=16)
    start_date = models.DateField(_('start date'))
    start_time = models.TimeField(_('start time'))
    duration = models.DurationField(_('duration'))
    price = models.DecimalField(_('price'), max_digits=11, decimal_places=2)
    call_id = models.IntegerField(_('call id'), db_index=True)
    bill = models.ForeignKey(
        'Bill',
        on_delete=models.PROTECT,
        related_name='calls',
        verbose_name=_('Bill')
    )
    call_price_kind = models.ForeignKey(
        'CallPriceKind',
        on_delete=models.PROTECT,
        related_name='calls',
        verbose_name=_('Call price kind')
    )

    class Meta:
        verbose_name = _('bill call record')
        verbose_name_plural = _('bill call records')

    def __str__(self):
        return f'{self.pk}'

    def __repr__(self):
        return f'<{self.pk}-{self.destination}>'


class CallPriceKind(models.Model):
    KINDS = (
        ('Standard', _('Standard time call')),
        ('Reduced', _('Reduced tariff time call')),
    )
    kind = models.CharField(_('kind'), choices=KINDS, max_length=16)
    start_time = models.TimeField(_('start time'))
    end_time = models.TimeField(_('end time'))
    minute_price = models.DecimalField(
        _('minute price'),
        max_digits=11,
        decimal_places=2
    )
    call_start_price = models.DecimalField(
        _('call start price'),
        max_digits=11,
        decimal_places=2
    )

    objects = CallPriceKindManager()

    class Meta:
        verbose_name = _('call price')
        verbose_name_plural = _('calls price')

    def __str__(self):
        return f'{self.pk}'

    def __repr__(self):
        return f'<{self.pk}-{self.kind}>'

    def calculate_price(self, duration):
        minutes = Decimal(duration.total_seconds() // 60)
        total_price = minutes * self.minute_price + self.call_start_price
        return total_price

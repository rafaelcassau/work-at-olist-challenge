from django.dispatch import Signal, receiver

from .tasks import process_record_task


process_record_signal = Signal(providing_args=['record'])


@receiver(process_record_signal)
def process_start_record_signal_callback(sender, record, **kwargs):
    process_record_task.delay(record.type, record.call_id)

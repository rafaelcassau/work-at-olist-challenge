from rest_framework.routers import DefaultRouter
from . import resources


router = DefaultRouter()

router.register(r'records', resources.AddRecordViewSet, base_name='records')
router.register(r'bills', resources.GetPhoneBillViewSet, base_name='bills')

urlpatterns = router.urls

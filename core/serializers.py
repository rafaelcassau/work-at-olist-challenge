from django.utils.translation import gettext as _

from rest_framework import serializers

from common.serializers.fields import PhoneNumberField, MonthYearField
from common.utils import RecordTypeEnum, get_last_close_period

from .models import RecordEvent, BillCallRecord


class RecordEventSerializer(serializers.ModelSerializer):
    source = PhoneNumberField(required=False)
    destination = PhoneNumberField(required=False)

    def validate(self, data):
        validated_data = super().validate(data)

        record_type = validated_data['type']
        start_record_type = RecordTypeEnum.START.name.lower()

        if record_type == start_record_type:
            source = validated_data.get('source')
            destination = validated_data.get('destination')

            if not source:
                raise serializers.ValidationError(_('source is required.'))
            if not destination:
                raise serializers.ValidationError(_('destination is required'))

        return validated_data

    class Meta:
        model = RecordEvent
        fields = ('type', 'call_id', 'timestamp', 'source', 'destination')


class SearchPhoneSerializer(serializers.Serializer):
    subscriber = PhoneNumberField()
    period = MonthYearField(required=False)

    def validate(self, data):
        last_period = get_last_close_period()
        data.setdefault('period', last_period)

        return data


class BillCallRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = BillCallRecord
        fields = (
            'destination',
            'start_date',
            'start_time',
            'duration',
            'price'
        )

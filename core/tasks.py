import logging

from webapp.celery import app


logger = logging.getLogger(__name__)


@app.task(bind=True)
def process_record_task(self, record_type, call_id):
    from .models import RecordEvent, Bill

    logger.info(
        f'start process_record_task ({record_type}-{call_id}) ' +
        f'- {self.request.id}.'
    )

    start_record, end_record = RecordEvent.objects.get_pair(call_id)
    if not start_record or not end_record:
        logger.info(
            f'skipped process_record_task, start or end record not found ' +
            f'start or end record not found - {self.request.id}'
        )
        return

    subscriber = start_record.source
    year = end_record.timestamp.year
    month = end_record.timestamp.month

    bill = Bill.objects.get_bill_by_period_and_subscriber(
        year,
        month,
        subscriber,
    )

    if not bill:
        bill = Bill.objects.create(
            year=year,
            month=month,
            subscriber=subscriber,
        )

    bill.add_record(start_record, end_record)

    logger.info(
        f'end process_record_task ({record_type}-{call_id}) ' +
        f'- {self.request.id}.'
    )

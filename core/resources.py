from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.schemas import AutoSchema

import coreapi

from .serializers import (
    RecordEventSerializer,
    SearchPhoneSerializer,
    BillCallRecordSerializer,
)
from .models import BillCallRecord


class AddRecordViewSet(viewsets.GenericViewSet):
    """
    create:
    Create a new record event, A record can be start or end type.
    """
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                'type',
                required=True,
                location='form',
                description='Type can be "start" or "end".'
            ),
            coreapi.Field(
                'call_id',
                required=True,
                location='form',
                description='Unique key pair identifier for ' +
                '"start" and end "records".'
            ),
            coreapi.Field(
                'timestamp',
                required=True,
                location='form',
                description='Records when the event happened.'
            ),
            coreapi.Field(
                'source',
                required=False,
                location='form',
                description='Origin phone number.'
            ),
            coreapi.Field(
                'destination',
                required=False,
                location='form',
                description='Destination phone number.'
            ),
        ]
    )

    http_method_names = ('post',)
    permission_classes = (IsAuthenticated,)
    serializer_class = RecordEventSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class GetPhoneBillViewSet(viewsets.GenericViewSet):
    """
    list:
    Return a list of bill record calls.
    """
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                'subscriber',
                required=True,
                location='query',
                description='Origin phone number.'
            ),
            coreapi.Field(
                'period',
                required=False,
                location='query',
                description='Bill period (month/year) ie. 12/2017.'
            ),
        ]
    )

    http_method_names = ('get',)
    lookups = ('subscriber', 'period')
    permission_classes = (IsAuthenticated,)
    serializer_class = BillCallRecordSerializer

    def get_queryset(self):
        queryset = BillCallRecord.objects.all()
        return queryset

    def get_lookups(self, request):
        valid_lookups = set(self.lookups) & set(request.query_params.keys())
        return valid_lookups

    def get_query_params(self, request, lookups):
        data = {}
        for lookup in lookups:
            data[lookup] = request.query_params.get(lookup)
        return data

    def validate(self, request):
        lookups = self.get_lookups(request)
        query_params = self.get_query_params(request, lookups)

        serializer = SearchPhoneSerializer(data=query_params)
        serializer.is_valid(raise_exception=True)

        return serializer.validated_data

    def list(self, request):
        validated_data = self.validate(request)

        subscriber = validated_data['subscriber']
        period = validated_data['period']

        bill_record_queryset = self.get_queryset()

        bill_record_queryset = bill_record_queryset.filter(
            bill__subscriber=subscriber,
            bill__year=period.year,
            bill__month=period.month
        ).order_by('-start_date', '-start_time')

        bill_record_queryset = self.paginate_queryset(bill_record_queryset)

        serializer = self.get_serializer(bill_record_queryset, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

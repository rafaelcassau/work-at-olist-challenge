from django.contrib import admin
from .models import RecordEvent, Bill, BillCallRecord, CallPriceKind


class RecordEventAdmin(admin.ModelAdmin):
    list_display = ('id', 'call_id', 'type', 'timestamp')
    readonly_fields = list_display + ('source', 'destination')
    list_filter = ('type', 'call_id')


class BillAdmin(admin.ModelAdmin):
    list_display = ('id', 'subscriber', 'year', 'month')
    readonly_fields = list_display
    list_filter = ('subscriber',)


class BillCallRecordAdmin(admin.ModelAdmin):
    list_display = ('id', 'destination', 'duration', 'price', 'call_id',)
    readonly_fields = list_display + (
        'start_date',
        'start_time',
        'bill',
        'call_price_kind',
    )
    list_filter = ('bill',)


class CallPriceKindAdmin(admin.ModelAdmin):
    list_display = ('id', 'kind', 'start_time', 'end_time')


admin.site.register(RecordEvent, RecordEventAdmin)
admin.site.register(Bill, BillAdmin)
admin.site.register(BillCallRecord, BillCallRecordAdmin)
admin.site.register(CallPriceKind, CallPriceKindAdmin)

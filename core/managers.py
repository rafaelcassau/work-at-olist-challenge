from datetime import datetime, timedelta

from django.db import models
from django.utils import timezone

from common.utils import RecordTypeEnum


class RecordEventManager(models.Manager):

    def get_pair(self, call_id):
        start_record = self.filter(
            call_id=call_id,
            type=RecordTypeEnum.START.name.lower()
        ).first()
        end_record = self.filter(
            call_id=call_id,
            type=RecordTypeEnum.END.name.lower()
        ).first()

        return start_record, end_record


class BillManager(models.Manager):

    def get_bill_by_period_and_subscriber(self, year, month, subscriber):
        current_bill = self.filter(
            year=year,
            month=month,
            subscriber=subscriber
        ).first()

        return current_bill


class CallPriceKindManager(models.Manager):

    def time_in_range(self, call_start_time, start_time_in, end_time_in):
        today = timezone.now().date()

        call = datetime.combine(today, call_start_time)

        start = datetime.combine(today, start_time_in)
        end = datetime.combine(today, end_time_in)

        if end <= start:
            end += timedelta(days=1)
        if call <= start:
            call += timedelta(days=1)

        return start <= call <= end

    def get_call_price_kind(self, call_start_time):
        queryset_call_price = self.all()
        for call_price in queryset_call_price:
            if self.time_in_range(
                call_start_time,
                call_price.start_time,
                call_price.end_time
            ):
                return call_price

        return None
